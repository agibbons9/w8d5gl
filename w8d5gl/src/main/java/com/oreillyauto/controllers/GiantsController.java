package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Giant;
import com.oreillyauto.service.GiantsService;

@Controller
public class GiantsController {
    @Autowired GiantsService WidgetsService;
    
   
    @GetMapping (value="/giants")
    public String giants(Model model) {
        return "giants";
    }
    

    @ResponseBody
    @GetMapping (value="/giants/getgiants")
    public List<Giant> getGiants() {
        return WidgetsService.getGiants();
    }
}
