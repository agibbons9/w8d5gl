<%@ include file="/WEB-INF/layouts/include.jsp" %>

<h1>Giants</h1>

<orly-table id="giantsTable" loaddataoncreate url="<c:url value='/giants/getgiants' />" includefilter
	bordered maxrows="10" tabletitle="Search Results" class="invisible">
	
	<orly-column field="" label="Action" class="">
	<div slot="cell">
   	    <%-- THE EASY STRAIGHT FORWARD WAY TO MANAGE A CELL!! (WITHOUT A CELL RENDERER) --%>
   	    <orly-icon color="cornflowerblue" name='edit'></orly-icon>
<orly-icon color="red" name='x'></orly-icon>
   	    
   	</div></orly-column>
	<orly-column field="id" label="ID" class="" sorttype="natural"></orly-column>
	<orly-column field="firstName" label="First Name"></orly-column>
	<orly-column field="lastName" label="Last Name"></orly-column>
	<orly-column field="height" label="Height (cm)"></orly-column>
	<orly-column field="dateOfBirth" label="Date of Birth">
		

		
	</orly-column>
</orly-table>
<script>
var giantsTable;
orly.ready.then((e)=> {
	giantsTable = orly.qid("giantsTable");
	addEventListeners();
	updateColumns();
});

function updateColumns(){
	giantsTable.updateColumn("dateOfBirth", "cellFn", function(row, cell, item) {                 	
	 	// Use the O'Reilly formatDate call
	 	return orly.formatDate(new Date(item.dateOfBirth), 'MM/DD/YYYY');
	});
}
function addEventListeners(){
	
}

</script>
